#include "debug.h"

FILE *debugFile = 0;

void debug_to_file(const char *fileName)
{
    debug_close();

    FILE *f = fopen(fileName, "a+");

    if (f)
        debugFile = f;
}

void debug_close(void)
{
    if (debugFile && (debugFile != stderr))
    {
        fclose(debugFile);
        debugFile = stderr;
    }
}