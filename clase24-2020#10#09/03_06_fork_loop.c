#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
  pid_t child_pid;

  printf("the main program process id is %d\n", (int)getpid());

  child_pid = fork();

  if (child_pid != 0)
  {
    printf("parent:: this is the parent process, me %d\n", (int)getpid());
    printf("parent:: this is the parent process, my parent %d\n", (int)getppid());
    printf("parent:: this is the parent process, my child %d\n", (int)child_pid);
    while (1)
    {
      printf("parent\n");
      sleep(1);
    }
  }
  else
  {
    printf("child:: this is the child process, me %d\n", (int)getpid());
    printf("child:: this is the child process, my parent %d\n", (int)getppid());
    while (1)
    {
      printf("child\n");
      sleep(1);
    }
  }
  return 0;
}
