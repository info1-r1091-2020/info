/*
 * Verificar con:
 *      gcc -E debug_test.c
 *      gcc -DDEBUG -E debug_test.c
 * 
 *      gcc -S -masm=intel debug_test.c 
 * 
 */
#include <stdio.h>

#include "debug.h"
#include "dummy.h"

int main()
{

#ifdef DEBUG
    debug_to_file("log.log");
#endif

    printf("Hello world\n");

    debug("Simple debug");
    info("Simple info");

    dummy();

#ifdef DEBUG
    debug_close();
#endif

    return 0;
}
