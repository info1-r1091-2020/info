struct autos
{
    char marca [20];
    char modelo [20];
    char color [20];
    int peso;
    int anio;
};


/* Criterios */
#define MARCA      0
#define MODELO     1
#define COLOR      2
#define PESO       3
#define ANIO       4

/*Formatos*/
#define CSV      0
#define RAW      1


struct autos * cargar_registro (int fd, char formato);
struct autos * nuevo_registro (void);
void ordenar_registros (struct autos **,char criterio);
void guardar_registros (int fd, struct autos **, char formato);
void remover_duplicados (struct autos **);


/*
 Equipos de desarrollo:
 Andrenacci - Sanchez -- cargar_registro
 Limachi - Yampa Potosi -- nuevo_registro
 Gallardo - Luparia -- ordenar_registros
 Allemand - Garcia M -- guardar_registros
 Cuda - Garcia Blanco -- remover_duplicados
 Lugones - Vega -- Main
 
 */
int main (void)
{
    struct autos * [100];
    
    
    
    
    
    return 0;
}

