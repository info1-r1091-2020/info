#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern FILE *debugFile;

void debug_to_file(const char *fileName);
void debug_close(void);

/*
 * Conditional Expression.	
 * If Condition is true ? then value X : otherwise value Y
 * 
 * */

#ifdef DEBUG
#define debug(fmt, ...) \
    fprintf((debugFile ? debugFile : stderr), "DEBUG:: %s[%d] %s() " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__);

#define info(fmt, ...) \
    fprintf((debugFile ? debugFile : stderr), "INFO :: %s[%d] %s() " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__);
#else
#define debug(fmt, ...) ((void)0)
#define info(fmt, ...) ((void)0)
#endif
