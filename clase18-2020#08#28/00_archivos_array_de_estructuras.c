#include <stdio.h>
#include <stdlib.h> //exit

struct st_data {
    int id;
    char desc[16];
};

typedef struct st_data ST_DATA;

int main(){
    int r;
    ST_DATA data_1 = {1, "valor 1"};
    ST_DATA data_2 = {2, "valor 2"};
    ST_DATA data_3 = {3, "valor 3"};
    ST_DATA data_4 = {4, "valor 4"};

    ST_DATA arr_data[4] = {data_1, data_2, data_3, data_4};


    //1 paso: abrir el archivo
    FILE *fp;

    fp = fopen("database.dat", "w");

    // 2 paso: verificar fopen
    if ( fp == NULL){
        printf("No se puedo abrir el archivos");
        exit(-1);
    }

    // 3 paso: leer o escribir
    r = fwrite( arr_data, sizeof(ST_DATA), 4, fp   );

    printf("Se escribieron %d bloque/s\n", r);

    // 4 paso: cerrar el archivo
    fclose(fp);

    return 0;

}

