# Informatica I

_Repositorio de la materia Informatica I de la Carrera de Electronica de la Universidad Tecnologica Nacional_

* [Calendario Academico](http://siga.frba.utn.edu.ar/up/docs/CalendarioAcademico2020.jpg) - Calendario Academico

## Entorno de Desarrollo 🛠️

_A continuacion se encuentra el listado de las herramientas necesarias para disponer el entorno de desarrollo, algunas de ellas son recomendaciones, que dependera de cada uno de ustedes utilizarlas:_



* [VirtualBox](https://www.virtualbox.org/) - Software de virtualización (opcional)
* [Ubuntu](https://ubuntu.com/) - Sistema Operativo Linux
* [Kate](https://kate-editor.org/) - Editor con Syntax Highlighting
* [gcc](https://gcc.gnu.org/) - The GNU Compiler
* [git](https://git-scm.com/) - Sistema de Control de Versiones
* [GitLab](https://gitlab.frba.utn.edu.ar/) - GitLab UTN

## Buenas Practicas

_Link colaborativo para ir registrando la lista de "Buenas Practicas - A trabajar!"_

	
* [Buenas Practicas Lenguaje C ](https://docs.google.com/document/d/19mOc3SUD3OH_fyS1547eLW-Fr_AQgNzZpofUqMSKPCQ/edit?usp=sharing) - Google Doc Buenas Prácticas

---

## Guias

* [Primeros pasos con un Sistema Operativo ](https://gitlab.com/info1-r1091-2020/public/-/blob/master/doc/Guia000-Linux.pdf)

* [Hola Mundo en profundidad ](https://gitlab.com/info1-r1091-2020/public/-/blob/master/doc/Guia001-HolaMundo.pdf)

---

## Temas 1° Parcial: Fecha 24/07/2020

### Sistemas de Numeracion 

- [x] Los sistemas de numeración y su evolución histórica. 
- [x] Sistemas de numeración decimal, binario, octal y hexadecimal. 
- [x] Pasajes entre sistemas de números enteros y positivos. 
- [x] Representación de Números signados: Convenio de signo y magnitud. 
- [x] Convenio de complemento a uno. 
- [x] Convenio de complemento a dos. 
- [x] Punto flotante. Precisión y truncado. 
- [x] Formato IEEE 754. 
- [x] Representación de caracteres: ASCII.

### Sistemas Operativos 

- [x] Conceptos Básicos sobre Sistemas Operativos modernos. Objeto y Funciones principales de un sistema operativo.


### El Compilador 

- [x] El Compilador. 
- [x] El Linker. Su relación con el Hardware y el Sistema Operativo. 
- [x] Introducción al desarrollo de software: Programa fuente, programa objeto, programa ejecutable. 
- [x] Fases de compilación y vinculación de programas, en el entorno de un sistema operativo

### Tipos de datos 

- [x] Tipos de datos, tamaño, y declaraciones. 
- [x] Constantes. Declaraciones. 

### Operadores 

- [x] Operadores de Asignación. 
- [x] Operadores aritméticos, relacionales y lógicos. 
- [x] Operadores de evaluación (expresiones condicionales). 
- [x] Operador Type Casting (cast). 
- [x] Jerarquía de operadores. Precedencia. 

### Preprocesador 

- [x] Preprocesador. 
- [x] Archivos de cabecera.


### Estructura de Selección e iteracion

- [x] Estructura de Selección: Proposición if. Proposición if-else. 
- [x] Una variante de Estructura de Selección: Selección Múltiple. Proposición switch-case. 
- [x] Estructura de Iteración: Ciclo while. Ciclo do-while. Ciclo for. Sentencias break y continue.

### Funciones

- [x] Funciones. 
- [x] Prototipo de una función. 
- [x] Argumentos. 
- [x] Archivos de cabecera (headers). 
- [x] Variables locales.
- [x] Variables globales
- [x] Clases de Almacenamiento. 
	- [x] Variables automaticas.
	- [x] Variables externas.
	- [x] Variables estáticas.
- [x] Reglas de alcance (scope). 
- [x] Proposición return. 
- [x] Llamada por valor y por referencia

### Arreglos

- [x] Arreglos. 
- [x] Declaración de arreglos. 
- [x] Concepto de vector (arreglo de una dimensión) y de matriz (arreglo de dos dimensiones). 
- [x] Inicialización de los arreglos. 
- [ ] Algoritmos de ordenamiento: intercambio o burbujeo, inserción, qsort, seleccion. Ordenamiento por más de un criterio. 
- [ ] Algoritmos de búsqueda en arreglos: secuencial y binaria 
- [ ] Algoritmos de inserción en arreglos.

### Cadena de caracteres

- [x] Fundamentos de string y carácter. 
- [x] Desarrollo de funciones asociadas
	- [x] strlen, strcpy, strcat, strcmp, strchr, strstr
	- [x] atoi, itoa


### Punteros

- [x] Concepto de puntero.
- [x] Concepto de dirección. 
- [x] Operadores unarios. 
- [x] Aritmética de punteros. 
- [x] Relación entre punteros y arreglos. 
- [x] Inicialización de punteros. 
- [x] Implementación de llamadas a función por referencia. 
- [x] Punteros a puntero. 
- [x] Vector de punteros. 

### Memoria dinamica

- [x] Reserva de memoria: malloc y free. 
- [x] Array dinamico. 
- [x] Array dinamico y funciones. 


### Argumentos del main()

- [x] Argumentos por línea de comandos. 

### Herramientas de Desarrollo

- [x] Makefile 

### Estructuras

- [x] Concepto de estructuras.
- [x] Estructuras y typedef.
- [x] Estructuras y funciones. Pasar por valor y retornar estructuras
- [x] Operaciones con estructuras ( = o copia ).
- [x] Operaciones con estructuras ( comparacion ).
- [x] Punteros a estructuras.
- [x] Funciones con punteros a estructuras.
- [x] Array de estructuras
- [x] Estructuras anidadas
- [x] Estructuras y memoria dinamica


### Archivos

- [x] Concepto de archivos (stream)
- [x] Archivos con stdio.h. Funciones: fopen, fclose, fread, fwrite, fseek. Ejemplos con texto y estructuras.
- [x] Archivos con stdio.h. Funciones: fprintf, fputs, fputc
- [x] Archivos con llamadas al sistema: read(), write(), lseek, fcntl e ioctl. 
- [] Dispositivos de E/S, Acceso a hardware: Dispositivo de audio.
- [x] Desarrollar aplicacion hexdump
- [x] Arhivos log
- [x] Arhivos csv

### Librerias
- [x] Librerias estaticas
- [x] Puntero a funcion
- [x] Librerias compartidas (shared)
- [x] Librerias de carga dinamica (dynamic loaded)


### Procesos
- [] Procesos. Introducción 
- [] Procesos. Creacion (system, fork, exec )
- [] Señales
- [] Procesos. Teminacion ( kill, wait ). Procesos "orphan" y "zombie"
- [] Procesos. Demonios 

### IPC Inter Process Communication

- [] Shared Memory
- [] Semaphores
- [] Mapped Memory
- [] Pipe ( unnamed )
- [] FIFO ( named pipe ). mknod(), mkfifo() 


### Sockets

- [] Introduccion 
- [] Conceptos de Networking
- [] Interfaz de sockets
