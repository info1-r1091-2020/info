/*
 * gcc file1.c file2.c -o ext
 *
 */

#include <stdio.h>

int i = 99;
extern int j;

int main(){
    printf(" i = %d | &i = %p\n", i, &i);
    printf(" j = %d | &j = %p\n", j, &j);

    return 0;
}