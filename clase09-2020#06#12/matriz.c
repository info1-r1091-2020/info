#include <stdio.h>

int det_3 (int [][3]);
void print_mtx (int mtx [][3]);

int main (void){
    
    int matriz[3][3];
    int i,j;
    int det;
    
    printf ("ingrese los valores de la matriz.\n");
    for(i=0;i<3;i++)
        for (j=0;j<3;j++)
        {
            printf("\ningrese M[%d][%d]",i,j);
            scanf("%d",&matriz[i][j]);
        }
    
    printf("\n\n");
    
    det= det_3 (matriz);
    printf("el determinante es: %d\n",det);
    
    printf("la matriz era:\n");
    print_mtx(matriz);
    return 0;
}


void print_mtx (int mtx [][3])
{
 
    int i,j;
    printf("\n\n");
    for(i=0;i<3;i++)
    {
        for (j=0;j<3;j++)
        {
            printf("\t%d", mtx[i][j]);
        }
        printf("\n\n");
    }
    
}


int det_3 (int mtx [][3])
{
    
    float det;
    int term1,term2,term3,term4,term5,term6;
    term1= mtx[0][0]*mtx[1][1]*mtx[2][2];
    term2= mtx[0][1]*mtx[1][2]*mtx[2][0];
    term3= mtx[0][2]*mtx[1][0]*mtx[2][1];
    term4= mtx[2][0]*mtx[1][1]*mtx[0][2];
    term5= mtx[2][1]*mtx[1][2]*mtx[0][0];
    term6= mtx[2][2]*mtx[1][0]*mtx[0][1];
        
    det = term1 + term2 + term3 - term4 - term5 - term6;
    
    return det;
}

