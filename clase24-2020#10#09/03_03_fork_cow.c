/*
 * COW == Copy On Write
 *
 */

#include <stdio.h>
int K;

int main(void)
{
    int i;
    int j;

    j = 200;
    K = 300;

    printf("Before forking: j = %d, K = %d  \n", j, K);
    i = fork();

    if (i > 0)
    { /* Delay the parent */
        sleep(1);
        printf("After forking, parent: j = %d, K = %d\n", j, K);
    }
    else
    {
        j++;
        K++;
        printf("After forking, child: j = %d, K = %d\n", j, K);
    }
    return 0;
}
