#include <stdio.h>
#include <stdlib.h> //exit

struct st_data {
    int id;
    char desc[16];
};

typedef struct st_data ST_DATA;

int main(){
    int r, i;

    //ST_DATA data;
    ST_DATA arr_data[4];
    
    //1 paso: abrir el archivo
    FILE *fp;

    fp = fopen("database.dat", "r");

    // 2 paso: verificar fopen
    if ( fp == NULL){
        printf("No se puedo abrir el archivo");
        exit(-1);
    }

    // 3 paso: leer 
    
    r = fread( arr_data, sizeof(ST_DATA), 4, fp   );
    
    printf("Cantidad de bloques leidos %d\n", r);

    for ( i=0; i< 4; i++){
        printf("id: %d\n", arr_data[i].id);
        printf("desc: %s\n", arr_data[i].desc);
    }
    

    // 4 paso: cerrar el archivo
    fclose(fp);

    return 0;

}
