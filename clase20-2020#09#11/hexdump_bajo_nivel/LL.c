#include<stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main (int argc, char ** argv)
{
    
    
    if (argc < 3)
    {
        printf("argumentos insuficientes");
        exit (0);
        
    }
    
    int fd_read, fd_write;
    char byte;
    char count=0;
    fd_read = open (argv [1],O_RDONLY);
    fd_write = open (argv [2],O_WRONLY | O_CREAT,0666);
    


    while (read (fd_read,&byte,1))
    {
        dprintf(1,"  %02x",byte);
        count ++;
        if (count == 15)
        {
            dprintf(1,"\n");
            count = 0;
            
        }
    }
    
    
    close (fd_read);
    close (fd_write);
   return 0; 
}

