# Clase #4

## Operadores Logicos

Se presentaron los operadores logicos y su combinacion con los operadores relacionales
para generar expresiones ( condiciones)

Pregunta:

	if ( 0 ) es VERDADERO?
	if ( 100 ) es VERDADERO?
	if ( -100 ) es VERDADERO?

## Estructuras de repetición

Se presentaron for / while / do while

Pregunta:

- Cuando conviene utilizar cada uno de ellos?

## scanf

Se realizaron varios ejercicios integrando:
 - printf
 - scanf
 - for / while / do while
 - break
 - continue

Repaso del concepto de direccion &

 # switch

Se presento esta estructura de seleccion y se realizo la comparacion con if else

Pregunta:

	float x = 10;
	
	switch (c) {

	es valido?

## Operador incremento y decremento

Se presentaron los operadores inc y dec, y las versiones de pre y post

Pregunta:

Con cuales de los operadores hay que tener especial cuidado al utilizarlo?


## Practica

Ejercicios con pequeños algoritmos:

- total
- minimo
- maximo
- promedio

AYUDA:

	printf("Continuar S/N: ");

	scanf(" %c", &sn);

*** para poder limpiar el buffer de stdin

## git y GitLab

[PASO PARA LA CLASE 5]

Se presento git como sistema de control de version, y la herramienta GitLab como repositorio remoto

Se practicaron los comandos basicos para:

- instalar git
- configurar git
- trabajar con repositorios locales
- trabajar con repositorios remotos






