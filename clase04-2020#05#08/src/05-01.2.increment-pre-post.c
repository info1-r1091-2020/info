// operador incremento, pre y post

#include <stdio.h>
int main()
{

    int i=0, j=0;

    i = 2;
    j = ++i;
    printf("j=++i = %d \n", j);

    i = 2;
    j = i++;
    printf("j=i++ = %d \n", j);


    return 0;
}
